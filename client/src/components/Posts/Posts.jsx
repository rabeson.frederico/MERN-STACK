import React from "react";
import Post from "./Post/Post";
import useStyles from "./styles";

const Posts = () => {
  // console.log(useStyles());
  const { actionDiv, mainContainer, smMargin } = useStyles();

  return (
    <React.Fragment>
      <h1>Posts</h1>
      <Post />
      <Post />
      <Post />
    </React.Fragment>
  );
};

export default Posts;
